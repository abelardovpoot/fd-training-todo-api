const Domain = require('../models/domain')

function check(req, res, next) {
    let domain = searchDomain(req)
    Domain.findOne({ domainName: domain }).then(dom => {
        res.locals.domain = dom

        return next()
    }).catch(err => {
        console.log(err);
        return res.status(404).send({ message: "domain not found" })
    })
}

function installApp(req, res, next) {
    let domain = searchDomain(req)
    res.locals.domain = domain;
    
    return next();

}

function searchDomain(req) {

    let domain = null;
    if (req.params.payload) {
        domain = req.params.payload;

    } else if (req.params.domain) {
        domain = req.params.domain;

    } else if (req.body.domainName) {
        domain = req.body.domainName;
    }

    if (domain.indexOf('.') >= 0) {
        domain = (domain.split('.')[0])
    }
    return domain;
}

module.exports = {
    check,
    installApp
};