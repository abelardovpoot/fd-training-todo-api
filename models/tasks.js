'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const TaskSchema = Schema({
    domain : {type:Schema.ObjectId, ref: 'Domain', required:true},
    ticketId : Number,
    name: String,
    completed: Boolean
});

module.exports = mongoose.model('Task',TaskSchema)