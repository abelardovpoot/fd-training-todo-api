'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const DomainSchema = Schema(
    {
        domainName: { type: String, unique: true }
    },
    { timestamps: true }
);

module.exports = mongoose.model('Domain', DomainSchema)

