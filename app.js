'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(cors());

//cargar rutas
const task_routes = require('./routes');
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/api', task_routes);



module.exports = app;