'use strict'

const Domain = require('../models/domain');

function saveDomain(req, res) {
    const { domain:domainName } = res.locals;
    const newDomain = new Domain({ domainName });

    newDomain.save((err, domainSaved) => {
        if (err) {
            //return domain if is duplicate
            console.log("ERROR", err)
            return res.status(500).send(err);
        }
        console.log(domainSaved);
        const domainString = JSON.stringify(domainSaved)
        res.send(domainString);
    })
}

module.exports = {
    saveDomain
};