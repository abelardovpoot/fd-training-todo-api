'use strict'

const Task = require('../models/tasks');


function pruebas(req, res) {
    res.status(200).send({
        message: 'probando controlador user'
    });
}

function getTasks(req, res) {
    const { id: ticketId } = req.params
    const { _id } = res.locals.domain
    Task.find({ ticketId, domain: _id }, function (err, tasks) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Tasks retrieved",
            data: tasks
        })
    });
}

function saveTask(req, res) {
    const { domain } = res.locals
    const { ticketId, name, completed } = req.body
    Task.create({domain, ticketId, name, completed})
        .then(task => res.status(201).send(task))
        .catch(() => res.status(500).send({ message: "error saving" }))
}

function editTask(req, res) {
    const taskId = req.params.id;
    const edit = req.body;
    Task.findByIdAndUpdate(taskId, edit, (err, taskEdited) => {
        if (err) {
            res.status(500).send({ message: "Error al actualizar la tarea" });
        } else {
            res.status(200).send({ task: taskEdited });
        }
    })
}

function deleteTask(req, res) {
    const taskId = req.params.id;
    Task.findByIdAndDelete(taskId, (err, taskDeleted) => {
        if (err) {
            res.status(500).send({ message: "Error al borrar la tarea" });
        } else {

            res.status(200).send({ task: taskDeleted });
        }
    });
}
module.exports = {
    pruebas,
    saveTask,
    editTask,
    deleteTask,
    getTasks
};