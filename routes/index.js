'use strict'
const express = require('express');
const TaskController = require('../controllers/taskController');
const DomainController = require('../controllers/domainController')
const router = express.Router();
const checkDomain = require('../middlewares/checkDomain')

router.post('/tasks', checkDomain.check, TaskController.saveTask);
router.put('/tasks/:id', TaskController.editTask);
router.delete('/tasks/:id', TaskController.deleteTask);
router.get('/:domain/tasks/:id', checkDomain.check, TaskController.getTasks);

router.post('/domain/:payload', checkDomain.installApp, DomainController.saveDomain);

module.exports = router;