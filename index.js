// Utilizar nuevas funcionalidades del Ecmascript 6
'use strict'
 
// Cargamos el módulo de mongoose para poder conectarnos a MongoDB
const mongoose = require('mongoose');
const app = require('./app');
const port = process.env.PORT || 3978;
const uri = 'mongodb+srv://mongouser:mongouser@clustertraining-bptnk.mongodb.net/test?retryWrites=true&w=majority'
// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;
// Usamos el método connect para conectarnos a nuestra base de datos
mongoose.connect(uri
    , {useNewUrlParser: true})
        .then(() => {
                // Cuando se realiza la conexión, lanzamos este mensaje por consola
            console.log('La conexión a MongoDB se ha realizado correctamente!!');
            app.listen(port, function(){
                console.log("Servidor del api Rest escuchando");
            });
        })
        .catch(err => console.log(err));
        // Si no se conecta correctamente mostramos el error
